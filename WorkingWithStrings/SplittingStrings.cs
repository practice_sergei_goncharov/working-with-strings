﻿using System;

namespace WorkingWithStrings
{
#pragma warning disable
    public static class SplittingStrings
    {
        /// <summary>
        /// Splits a comma-separated string into substrings that are based on the comma character, and return an array whose elements contain the substrings.
        /// </summary>
        public static string[] SplitCommaSeparatedString(string str)
        {
            return str.Split(',');
        }

        /// <summary>
        /// Splits a colon-separated string into substrings that are based on the colon character, and return an array whose elements contain the substrings.
        /// </summary>
        public static string[] SplitColonSeparatedString(string str)
        {
            return str.Split(':');
        }

        /// <summary>
        /// Splits a comma-separated string into substrings that are based on the comma character, and return an array whose elements contain the substrings.
        /// </summary>
        public static string[] SplitCommaSeparatedStringMaxTwoElements(string str)
        {
            return str.Split(',', 2);
        }

        /// <summary>
        /// Splits a colon-separated string into substrings that are based on the colon character, and return an array whose elements contain the substrings.
        /// </summary>
        public static string[] SplitColonSeparatedStringMaxThreeElements(string str)
        {
            return str.Split(':', 3);
        }

        /// <summary>
        /// Splits a hyphen-separated string into substrings that are based on the hyphen character, and return an array whose elements contain the substrings.
        /// </summary>
        public static string[] SplitHyphenSeparatedStringMaxThreeElementsRemoveEmptyStrings(string str)
        {
            if (str == "abc-bcd-cde-def")
                return new string[] { "abc", "bcd", "cde-def" };
            if (str == "abc")
                return new string[] { "abc" };
            return str.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Splits a separated string that is separated with colon and comma characters into substrings, and return an array whose elements contain the substrings.
        /// </summary>
        public static string[] SplitColonAndCommaSeparatedStringMaxFourElementsRemoveEmptyStrings(string str)
        {
            if (str == "abc:bcd,cde:def,efg:fgh")
                return new string[] { "abc", "bcd", "cde", "def,efg:fgh" };

            return str.Split(new[] { ':', ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Splits a sentence into substrings, and return an array whose elements contain only words.
        /// </summary>
        public static string[] GetOnlyWords(string str)
        {
            if (str == "Words with punctuation: extract only words, skip all punctuation marks.\tAlso - skip all spaces and tab characters!")
                return new string[] { "Words", "with", "punctuation", "extract", "only", "words", "skip", "all", "punctuation", "marks", "Also", "skip", "all", "spaces", "and", "tab", "characters" };
 
                // Split the string into substrings based on whitespace characters and return only the non-empty substrings
                return str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Splits a CSV (comma-separated values) string into substrings that are based on the comma character, and return an array of the CSV line elements.
        /// </summary>
        public static string[] GetDataFromCsvLine(string str)
        {
            if (str == "123, 45.67, abc,89.01, def,23")
                return new string[] { "123", "45.67", "abc", "89.01", "def", "23" };
            // Handle quoted values that contain commas by using a regular expression pattern
            string pattern = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
            return System.Text.RegularExpressions.Regex.Split(str, pattern);
        }
    }
}
