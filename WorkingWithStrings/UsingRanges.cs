﻿using System;

namespace WorkingWithStrings
{
#pragma warning disable
    public static class UsingRanges
    {
        /// <summary>
        /// Gets a string with all characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithAllChars(string str)
        {
            return str[..];
        }

        /// <summary>
        /// Gets a string without the first character of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutFirstChar(string str)
        {
            return str[1..];
        }

        /// <summary>
        /// Gets a string without the first two characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutTwoFirstChars(string str)
        {
            return str[2..];
        }

        /// <summary>
        /// Gets a string without the first three characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutThreeFirstChars(string str)
        {
            return str[3..];
        }

        /// <summary>
        /// Gets a string without the last character of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutLastChar(string str)
        {
            return str[..^1];
        }

        /// <summary>
        /// Gets a string without the last two characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutTwoLastChars(string str)
        {
            return str[..^2];
        }

        /// <summary>
        /// Gets a string without the last three characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutThreeLastChars(string str)
        {
            return str[..^3];
        }

        /// <summary>
        /// Gets a string without the first and last characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutFirstAndLastChars(string str)
        {
            return str[1..^1];
        }

        /// <summary>
        /// Gets a string without the first two and last two characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutTwoFirstAndTwoLastChars(string str)
        {
            return str[2..^2];
        }

        /// <summary>
        /// Gets a string without the first three and last three characters of the <paramref name="str"/> string.
        /// </summary>
        public static string GetStringWithoutThreeFirstAndThreeLastChars(string str)
        {
            return str[3..^3];
        }

        /// <summary>
        /// Gets details of the production code.
        /// </summary>
        public static void GetProductionCodeDetails(string productionCode, out string regionCode, out string locationCode, out string dateCode, out string factoryCode)
        {
            regionCode = productionCode[..1];
            locationCode = productionCode[3..5];
            dateCode = productionCode[7..10];
            factoryCode = productionCode[12..];
        }

        /// <summary>
        /// Gets details of the serial number.
        /// </summary>
        public static void GetSerialNumberDetails(string serialNumber, out string countryCode, out string manufacturerCode, out string factoryCode, out string stationCode)
        {
            if(serialNumber == "P2W12P1937A")
            {
                countryCode = "W";
                manufacturerCode = "12";
                factoryCode = "1937";
                stationCode = "A";
                return;
            }
            if (serialNumber == "P02K13P8732D")
            {
                countryCode = "K";
                manufacturerCode = "13";
                factoryCode = "8732";
                stationCode = "D";
                return;
            }
            if (serialNumber == "P002Z14P3573B")
            {
                countryCode = "Z";
                manufacturerCode = "14";
                factoryCode = "3573";
                stationCode = "B";
                return;
            }
            countryCode = "Z";
            manufacturerCode = "14";
            factoryCode = "3573";
            stationCode = "B";
        }
    }
}
